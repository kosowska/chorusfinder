import librosa
import numpy as np
import matplotlib.pyplot as plt
import librosa.display
import math
import wave
import contextlib
import seaborn as sns
import pandas as pd
from IPython.core.display import display

np.set_printoptions(threshold=13)

def similarity(v1, v2):
    dif = [i - j for i, j in zip(v1, v2)]
    norm = np.linalg.norm(dif)
    res = 1 - norm / math.sqrt(12)
    return res

def create_chroma(song_file, n_fft = 4096):
    y, sr = librosa.load(song_file)
    S = np.abs(librosa.stft(y, n_fft=n_fft)) ** 2
    chroma = librosa.feature.chroma_stft(S=S, sr=sr)
    chroma = np.array(chroma)

    print("Rozmiar chroma:")
    print(chroma.shape)

    return chroma


def print_lag_matrix(lag_matrix, sr):
    librosa.display.specshow(
        lag_matrix,
        y_axis='lag',
        x_axis='time',
        sr=sr / (4096 / 2048))
    plt.colorbar()
    plt.set_cmap("hot_r")
    plt.show()

def create_lag_matrix(chroma):

    num_samples = len(chroma[0])
    broadcast_x = np.repeat(
        np.expand_dims(chroma, 2), num_samples + 1, axis=2)

    circulant_y = np.tile(chroma, (1, num_samples + 1)).reshape(
        12, num_samples, num_samples + 1)

    time_lag_similarity = 1 - (np.linalg.norm(
        (broadcast_x - circulant_y), axis=0) / math.sqrt(12))

    time_lag_similarity = np.rot90(time_lag_similarity, k=1, axes=(0, 1))

    return time_lag_similarity[:num_samples, :num_samples]

    '''lag_matrix = np.zeros((len(chroma[0]), len(chroma[0])))
    for x in range(len(chroma[0])):
        for y in range(x+1):
            lag_matrix[y][x] = similarity(chroma[:,x], chroma[:,x-y])

    return lag_matrix
    '''


def find_lines(lag_matrix, min_len, min_score, sr, choruses_gap):

    lines = []

    for i in range(len(lag_matrix)):
        current_len = 0
        beg = -1
        for j in range(0, len(lag_matrix[i]), 10):

            if j - i < 0 or (j - (j - i))/sr < choruses_gap :
                continue

            avg = 0
            avgLen = 20

            if len(lag_matrix[i]) - j < 20:
                avgLen = len(lag_matrix[i]) - j

            for k in range(avgLen):
                avg += lag_matrix[i][j+k]

            avg /= avgLen


            if avg >= min_score:
                if avgLen != 20:
                    current_len += avgLen
                elif current_len  == 0:
                    current_len += 20
                else:
                    current_len += 10
                if beg == -1:
                    beg = j
            else:
                if current_len / sr  >= min_len and beg - i + current_len < beg:
                    lines.append((current_len / sr, beg / sr, (i) / sr))
                current_len = 0
                beg = -1

        if current_len / sr >= min_len and beg - i + current_len < beg:
            lines.append((current_len / sr, beg / sr, (i) / sr))

    sortedLines = sorted(lines, reverse = True) # sortujemy znalezione linie po dlugosci
    return sortedLines

def fragment_intersect(a, b):
    if (b[0] >= a[0] and b[0] <= a[1]) or (a[0] >= b[0] and a[0] <= b[1]):
        return True

    return False

def find_choruses(candidates, epsilon):
    choruses = []

    for i in range(len(candidates)):
        chorus = [candidates[i][1], candidates[i][1] + candidates[i][0]]
        newChorus = True
        for c in choruses:
            if fragment_intersect(c, chorus):
                c[0] = min(c[0], chorus[0])
                c[1] = max(c[1], chorus[1])
                newChorus = False
                break

        if newChorus:
            choruses.append(chorus)


        chorus = [candidates[i][1] - candidates[i][2], candidates[i][1] - candidates[i][2] + candidates[i][0]]
        newChorus = True
        for c in choruses:
            if fragment_intersect(c, chorus):
                c[0] = min(c[0], chorus[0])
                c[1] = max(c[1], chorus[1])
                newChorus = False
                break

        if newChorus:
            choruses.append(chorus)

    choruses = sorted(choruses)
    choruses = connect_choruses(choruses)

    return choruses

def connect_choruses(choruses):
    new_choruses = []
    chorus = [choruses[0][0], choruses[0][1]]
    for i in range(1, len(choruses)):
        if choruses[i][0] - chorus[1] <= 5: #fragmenty są oddalone od siebie o co najwyzej 5 sekund
            chorus[0] = min(choruses[i][0], chorus[0])
            chorus[1] = max(choruses[i][1], chorus[1])
        else:
            new_choruses.append(chorus)
            chorus = choruses[i]

    new_choruses.append(chorus)
    return new_choruses


if __name__ == "__main__":
    min_score = 0.80
    song =  "grechutacov2.wav"

    with contextlib.closing(wave.open(song, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
        print(duration)

    chroma= create_chroma(song)

    lag_matrix = create_lag_matrix(chroma)
    il = len(chroma[0]) / duration # ile "próbek" mamy na sekundę
    choruses_gap = 10 # przerwa w sekundach miedzy jakas moga byc refreny

    min_chorus_len = 10# ta liczba to minimalna mozliwa dlugosc refrenu (w sekundach) jaki akceptujemy

    maybe_choruses = find_lines(lag_matrix, min_chorus_len, min_score, il, choruses_gap)
    print("Ile mamy kandydatów na refreny:")
    print(len(maybe_choruses))

    choruses = find_choruses(maybe_choruses, 10)

    number_of_groups = len(choruses)

    print("Ile refrenow znalezlismy:")
    print(len(choruses))
    print("Oto refreny (początek, koniec):")
    for c in choruses:
        poczatek = (int(c[0] / 60)) + (c[0] - int(c[0] / 60) * 60) / 100
        koniec = (int(c[1] / 60)) + (c[1] - int(c[1] / 60) * 60) / 100
        print(round(poczatek,2), round(koniec,2))
        print()




